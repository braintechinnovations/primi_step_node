const express = require('express')
const mongoose = require('mongoose')
const ejs = require('ejs')
const bodyparser = require('body-parser')

const BlogPost = require('./models/BlogPost')

const app = express()
app.use(express.static('public'))
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
app.set('view engine', 'ejs')

const address = "127.0.0.1"
const port = 4000

mongoose.connect("mongodb://localhost:27017/blog_db", {useNewUrlParser: true})

app.listen(port, address, () => {
    console.log(`Sono connesso alla porta ${port}`)
})

app.get("/post/new", (req, res) => {
    res.render('create')
})

//Soluzione con Callbacks
app.post("/post/save", (req, res) => {

    var postItem = {
        title: req.body.input_title,
        body: req.body.input_body
    }

    // res.json(postItem)

    BlogPost.create(postItem, (errore, documento) => {
        if(errore){
            res.json({
                status: "ERRORE"
            })
        }
        else{
            res.json(documento)
        }

    })

})

app.post("/post/save", async (req, res) => {

    var postItem = {
        title: req.body.input_title,
        body: req.body.input_body
    }

    //TODO: Try Catch Errore
    var blogPostInserito = await BlogPost.create(postItem);
    // res.json(blogPostInserito);

    res.redirect("/")

})

app.get("/", async (req, res) => {
    var postList = await BlogPost.find({});
    console.log(postList)

    res.render('index', {
        postList: postList
    })
})

app.get("/post/detail/:id", async (req, res) => {
    console.log(req.params)

    var post = await BlogPost.findById(req.params.id)
    res.render('post', {
        blogpost: post
    })
})

//TODO: Modifica
//TODO: Eliminazione