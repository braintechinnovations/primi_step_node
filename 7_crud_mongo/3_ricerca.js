const mongoose = require('mongoose')
const BlogPost = require('./models/BlogPost')

mongoose.connect("mongodb://localhost:27017/blog_db", {useNewUrlParser: true})

// findAll, molto simile al SELECT * FROM BlogPost di MySQL senza WHERE clause
// Commento rapido Ctrl + k + c
// BlogPost.find({}, (error, document) => {
//     console.log(error, document)
// })

// Ricerca puntuale, simile a SELECT * FROM BlogPost WHERE title = "Lasagne e cannelloni"
// BlogPost.find({
//     title: "Lasagne e cannelloni"
// }, (error, document) => {
//     console.log(error, document)
// })

// Ricerca per LIKE
// Se utilizzo /gne/ è equivalente a: SELECT * FROM BlogPost WHERE title LIKE '%gne%'
// Se utilizzo /^gne/ è equivalente a: SELECT * FROM BlogPost WHERE title LIKE 'gne%'
// Se utilizzo /gne$/ è equivalente a: SELECT * FROM BlogPost WHERE title LIKE '%gne'
BlogPost.find({
    title: /lloni$/
}, (error, document) => {
    console.log(error, document)
})