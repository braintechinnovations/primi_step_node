const express = require('express')
const path = require('path')
const app = express()

app.use(express.static('public'))

const address = "127.0.0.1"
const port = 4000

app.listen(port, address, () => {
    console.log(`Sono connesso alla porta ${port}`)
})

app.get("/", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "index.html")
    )
})

app.get("/chi-siamo", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "chisiamo.html")
    )
})

app.get("/contatti", (req, res) => {
    res.sendFile(
        path.resolve(__dirname, "contatti.html")
    )
})