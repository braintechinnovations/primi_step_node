const express = require('express')
const path = require('path')
const ejs = require('ejs')

const app = express()
app.use(express.static('public'))           //Utilizzo un Middleware
app.set('view engine', 'ejs')               //Set di una impostazione interna in Express

const address = "127.0.0.1"
const port = 4000

app.listen(port, address, () => {
    console.log(`Sono connesso alla porta ${port}`)
})

app.get("/", (req, res) => {
    res.render('index')
})

app.get("/about", (req, res) => {
    res.render('about')
})

app.get("/post", (req, res) => {
    res.render('post')
})

app.get("/contact", (req, res) => {
    res.render('contact')
})