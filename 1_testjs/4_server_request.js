const http = require('http')

//Commento veloce Ctrl + k + c
const server = http.createServer( (req, res) => {
    // console.log(req.url)

    if(req.url == "/chi-siamo"){
        res.end("Pagina chi siamo")
    } else if (req.url == "/contatti") {
        res.end("Pagina Contatti")
    } else{
        res.writeHead(404)
        res.end("Ooops... Non trovato!")
    }
} )

server.listen(4000);  