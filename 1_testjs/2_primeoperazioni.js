//Questo è un commento, non viene eseguito da V8

//var a = 5;
//console.log(a); 

//Somma di due numeri
//Con "Ctrl + k + c" faccio un commento rapido!
//Con "Ctrl + k + u" decommento il codice commentato.
// var a = 5;
// var b = 6;
// var somma = a + b;

// console.log(somma);

//Concatenazione di stringhe
// var nome = "Giovanni"
// var cognome = "Pace"
// console.log(nome + " " + cognome)

//Concatenazione di Stringhe
// var nome = "Giovanni"
// var eta = 34
// // console.log(nome + " " + eta + 1)      //Giovanni 341
// // console.log(eta + 1 + " " + nome)      //35 Giovanni
// console.log(nome + " " + (eta + 1))

//Oggetto semplice JavaScript
var persona = {
    nome: "Giovanni",
    cognome: "Pace",
    eta: 34 
}

console.log(persona.cognome);

