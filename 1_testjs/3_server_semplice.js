/**
 * Il comando require include il modulo http.js all'interno del mio "scope globale" e l'ho reso reperibile
 * grazie alla costante "http" (è costante quindi immutabile perché una libreria non cambia il proprio contenuto
 * e non ha senso di dichiararla come una variabiles)
 */
const http = require('http')

/**
 * Definisco e quindi creo un server a partire dalla libreria http che ho incluso in precedenza,
 * definisco due parametri, request e response che equivalgono alle relative tematiche http illustrate.
 */
const server = http.createServer( (req, res) => {
    console.log("Ciao Console!");                       //Output a livello server
    res.end("Ciao Giovanni!")                           //Response che va su CHROME!
} )

server.listen(4000);                                    //Il server è in ascolto sulla porta 4000

//Per avviare il server aprire il terminale di Visual Studio Code "Terminal -> New Terminal"
//Entro nella cartella specifica "cd .\1_testjs\"
//Avvio il server con "node .\3_server_semplice.js"
//Per terminare il server, selezioniamo il terminale e pigiamo i tasti Ctrl + c