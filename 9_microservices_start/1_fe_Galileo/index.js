const express = require('express')
const ejs = require('ejs')
const bodyparser = require('body-parser')
const axios = require('axios')

const app = express()
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))

app.set('view engine', 'ejs')

const address = "127.0.0.1"
const port = 4001

app.listen(port, address, () => {
    console.log("Galileo è in ascolto sulla porta " + port)
})

//Metodo per la visualizzazione del form di inserimento
app.get("/", (req, res) => {
    res.render('form_inserimento')
})

//Metodo di invio dati a Serrat e recupero della risposta sempre da Serrat
app.post("/inserisci", (req, res) => {
    // res.json(req.body)

    var persona = {
        nome: req.body.input_nome,
        cognome: req.body.input_cognome,
        telefono: req.body.input_telefono
    }

    axios.post("http://localhost:4002/salva", persona)
    .then(
        function(response){
            console.log(response)
            res.json(response.data)
        }
    )
    .catch(
        function(errore){
            console.log(errore)
            res.json({
                status: "ERRORE"
            })
        }
    )
})

app.get("/lista", (req, res) => {

    axios.post("http://localhost:4002/elenco", {})
    .then(
        function(response){
            res.render('elenco_contatti', {
                contatti: response.data
            })
        }
    )
    .catch(
        function(errore){
            console.log(errore)
            res.json({
                status: "ERRORE",
                data: errore
            })
        }
    )

})
