const express = require('express')
const bodyparser = require('body-parser')
const mongoose = require('mongoose')
const Contatto = require('./models/Contatto')

const app = express()
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))

const address = "127.0.0.1"
const port = 4002

mongoose.connect('mongodb://localhost:27017/rubrica', {useNewUrlParser: true})

app.listen(port, address, () => {
    console.log("Serrat è in ascolto sulla porta " + port)
})

app.post("/salva", async (req, res) => {
    console.log(req.body)

    var personaSalvata = await Contatto.create(req.body);
    console.log(personaSalvata);

    res.json(personaSalvata)
})

app.post("/elenco", async (req, res) => {

    var filtro = req.body

    var elencoPersone = await Contatto.find(filtro)

    res.json(elencoPersone)

})