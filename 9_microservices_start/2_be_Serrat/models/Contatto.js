const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ContattoSchema = new Schema({
    nome: String,
    cognome: String,
    telefono: String 
})

const Contatto = mongoose.model('Contatto', ContattoSchema)

module.exports = Contatto