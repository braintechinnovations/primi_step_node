const http = require('http')
const fs = require('fs')

const homePage = fs.readFileSync('index.html');
const chisiamoPage = fs.readFileSync('chisiamo.html');
const contattiPage = fs.readFileSync('contatti.html');
const notfoundPage = fs.readFileSync('notfound.html');

const server = http.createServer((req, res) => {

    switch(req.url){
        case '/':
            // res.end("Sei sulla pagina index");
            res.end(homePage);
            break;
        case '/chi-siamo':
            // res.end("Sei sulla pagina chi siamo");
            res.end(chisiamoPage);
            break;
        case '/contatti':
            // res.end("Sei sulla pagina contatti");
            res.end(contattiPage);
            break;
        default:
            res.writeHead(404);
            // res.end("<h1>Ooops... non trovato ;(</h1>")
            res.end(notfoundPage);
            break;
    }

})

server.listen(4000)