const express = require('express')  //Inclusione del modulo Express

const app = express()               //Inizializzazione dell'applicazione Express

app.get("/", (req, res) => {
    res.end("Sono la pagina Index")
})

app.get("/chi-siamo", (req, res) => {
    res.end("Sono la pagina Chi siamo")
})

app.get("/contatti", (req, res) => {
    res.end("Sono la pagina Contatti")
})

app.listen(4000, () => {
    console.log("Ciao, sono Express in ascolto sulla porta 4000")
});

