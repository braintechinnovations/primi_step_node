const express = require('express')

const app = express()

const address = "127.0.0.1"
const port = 4001

app.listen(port, address, () => {
    // console.log("Ciao sono connesso alla porta " + port);
    console.log(`Ciao sono connesso alla porta ${port}`);
})

app.get("/chi-siamo", (req, res) => {
    var contenuto = {
        status: "SUCCESS",
        page: "chi-siamo"
    }

    res.json(contenuto)
})

app.get("/contatti", (req, res) => {
    var contenuto = {
        status: "SUCCESS",
        page: "contatti"
    }

    res.json(contenuto)
})

app.get("/", (req, res) => {
    var contenuto = {
        status: "SUCCESS",
        page: "root"
    }

    res.json(contenuto)
})